import {BASE_URL} from './config'

function loginAPI (user, callback) {
  fetch(`${BASE_URL}/api/auth/login`, {
    method: 'POST',
    credentials: 'same-origin',
    body: JSON.stringify(user)
  })
    .then(res => res.json())
    .then(res => typeof callback === 'function' && callback(res))
}

export {
  loginAPI
}
