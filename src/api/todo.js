import { BASE_URL } from './config'

function addTodo (todo, callback) {
  fetch(`${BASE_URL}/api/todo/add`, {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`
    },
    body: JSON.stringify(todo)
  })
    .then(res => res.json())
    .then(res => typeof callback === 'function' && callback(res))
}

function editTodo (todo, callback) {
  fetch(`${BASE_URL}/api/todo/detail/${todo.id_todo}`, {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`
    },
    body: JSON.stringify(todo)
  })
    .then(res => res.json())
    .then(res => typeof callback === 'function' && callback(res))
}

function removeTodo (todoid, callback) {
  fetch(`${BASE_URL}/api/todo/detail/${todoid}`, {
    method: 'DELETE',
    credentials: 'same-origin',
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`
    }
  })
    .then(res => res.json())
    .then(res => typeof callback === 'function' && callback(res))
}

function getTodos (callback) {
  fetch(`${BASE_URL}/api/todo/all`, {
    method: 'GET',
    credentials: 'same-origin',
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`
    }
  }).then(res => res.json())
    .then(res => typeof callback === 'function' && callback(res))
}

function getTodo (id, callback) {
  fetch(`${BASE_URL}/api/todo/detail/${id}`, {
    method: 'GET',
    credentials: 'same-origin',
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`
    }
  })
    .then(res => res.json())
    .then(res => typeof callback === 'function' && callback(res))
}

export {
  addTodo,
  getTodos,
  getTodo,
  editTodo,
  removeTodo
}
